<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTags extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('tags', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('event_id')->unsigned();
      $table->integer('tag_id')->unsigned();
      $table->timestamps();
      $table
        ->foreign('event_id')
        ->references('id')
        ->on('events')
        ->onDelete('cascade');
      $table
        ->foreign('tag_id')
        ->references('id')
        ->on('admin_tags')
        ->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('tags');
  }
}
